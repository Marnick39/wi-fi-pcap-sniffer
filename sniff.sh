#!/bin/bash
# ---- Settings ----
channel=11
secondsBeforeReset=5
# ------------------


sniffCmd="screen -S airodump-ng -dm bash -c 'sudo airodump-ng -c $channel -w capture  wlan0mon --uptime --output-format pcap --write-interval 1'"
recentFileCmd="ls *.cap | tail -n 1"
notGrown=0

clear
echo '####################################'
echo '-------- PREPPING INTERFACE --------'
echo '####################################'
#sudo systemctl stop wpa_supplicant
#sudo ifconfig wlan0 down
sudo airmon-ng start wlan0
echo '####################################'
echo '-------- ACTIVATING SNIFFER --------'
echo '####################################'

echo -n "Starting airodump-ng."
eval $sniffCmd
echo -n "." ;sleep 1
echo -n ". ";sleep 1
echo done!

currentSize=$(stat --printf="%s" $(eval $recentFileCmd))
echo '####################################'
echo '---- ENGAGING BREAKAGE DETECTOR ----'
echo '####################################'
while true; do 
  sleep 1
  recentSize=$(stat --printf="%s" $(eval $recentFileCmd))
  if (( $currentSize < $recentSize )); then
    #echo file has grown!
    notGrown=0
  else
    notGrown=$(($notGrown + 1 ))
    echo File has not grown for $notGrown seconds!
  fi

  if (( $notGrown >= $secondsBeforeReset )); then
    echo  "           >> RESETTING <<"
    notGrown=0
    #TODO: Improve kill command (make screen kill using the name)
    screen -X -S airodump-ng quit
    sudo ifconfig wlan0mon down
    sudo ifconfig wlan0mon up
    eval $sniffCmd
    sleep 2
  fi

  currentSize=$recentSize
done
